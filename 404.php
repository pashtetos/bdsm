<?php
	get_header();
	global $wow_settings;
?>
<div class="container page-404">
	<div class="row">
		<?php if(isset($wow_settings['img-404']) && $wow_settings['img-404']) : ?>
			<div class="col-lg-6 col-md-6 col-xs-12 left">  
				<div class="img-404">
					<img src="<?php echo esc_url($wow_settings['img-404']['url']); ?>">
				</div>
			</div>
		<?php endif; ?>
		<div class="<?php if(isset($wow_settings['img-404']) && $wow_settings['img-404']){ echo "col-lg-6 col-md-6"; } ?>  col-xs-12 right">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
					<header class="page-header">
						<h2 class="page-title"> <?php echo esc_html__('Ой!', 'btsm');  ?> </h2>
					</header>
					<div class="page-content">
						<div class="content-404">
							<p><?php echo  esc_html__('Можливо, ви неправильно вказали адресу сайту.', 'btsm'); ?></p>
							<div class="btn-404">
								<a href="<?php echo esc_url( home_url('/') ); ?>"><?php echo esc_html__('Повернутися на головну','wow'); ?></a>
							</div>
						</div>
					</div><!-- .page-content -->

				</div><!-- #content -->
			</div><!-- #primary -->		
		</div>
	</div>
</div>

<?php
get_footer();