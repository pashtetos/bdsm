<?php

add_action('wp_enqueue_scripts', 'wow_child_css', 1001);

// Load CSS
function wow_child_css() {
    // wow child theme styles
    wp_register_style( 'styles-child', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_style( 'styles-child' );
}

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

unset( $tabs['reviews'] ); // Убираем вкладку "Отзывы"
unset( $tabs['additional_information'] ); // Убираем вкладку "Свойства"

return $tabs;

}

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );

function btsm_post_product_metabox()
{
    $cmb = new_cmb2_box( array(
        'id'            => 'btsm_metabox',
        'title'         => __( 'Блок', 'cmb2' ),
        'object_types'  => array( 'product', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );
//    $cmb->add_field( array(
//        'name'             => 'Тип фасаду',
//        'desc'             => 'Select an option',
//        'id'               => 'wiki_test_select',
//        'type'             => 'select',
//        'show_option_none' => true,
//        'default'          => 'custom',
//        'options'          => array(
//            'standard' => __( 'МДФ ПВХ', 'cmb2' ),
//            'custom'   => __( 'МДФ рамочний', 'cmb2' ),
//            'none'     => __( 'Не вказаний', 'cmb2' ),
//        ),
//        'default' => 'custom',
//    ) );
    $cmb->add_field( array(
        'name' => 'Повна назва товару',
        'id' => 'btsm_title',
        'type' => 'text'
    ) );


}

add_action('cmb2_admin_init', 'btsm_post_product_metabox');

function custom_woocommerce_catalog_orderby( $orderby ) {
unset($orderby["price"]); 
unset($orderby["price-desc"]);
return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "custom_woocommerce_catalog_orderby", 20 );



add_action( 'init', 'custom_taxonomy_fasad_type' );
function custom_taxonomy_fasad_type()  {
    $labels = array(
        'name'                       => 'Тип фасаду',
        'singular_name'              => 'Тип фасаду',
        'menu_name'                  => 'Тип фасаду',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'separate_items_with_commas' => 'Separate Item with commas',
        'search_items'               => 'Search Items',
        'add_or_remove_items'        => 'Add or remove Items',
        'choose_from_most_used'      => 'Choose from the most used Items',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'fasad_type', 'product', $args );
    register_taxonomy_for_object_type( 'fasad_type', 'product' );
}