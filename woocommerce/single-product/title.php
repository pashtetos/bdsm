<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>
    <h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
    <div class="facade_type">
        <?php
        $terms = wp_get_post_terms(
            get_the_ID(),
            'fasad_type',
            array("fields" => "names")
        );
        foreach ($terms as $term) {
                echo __('Тип фасаду: ', 'btsm');
                echo $term;
        }
        ?></div>
    <h4><?php $btsm_title = get_post_meta(get_the_ID(), 'btsm_title', true);
echo esc_html($btsm_title); ?></h4><?php


?>