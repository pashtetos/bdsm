<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

if (!$product || !$product->is_visible()) {
    return;
}

$class_item_product = '';
if (!is_ajax())
    $class_item_product = wow_get_class_item_product();

?>
<li <?php post_class($class_item_product); ?>>
    <div class="products-entry clearfix product-wapper">
        <?php do_action('woocommerce_before_shop_loop_item'); ?>
        <div class="products-thumb">
            <?php
            /**
             * woocommerce_before_shop_loop_item_title hook
             *
             * @hooked woocommerce_show_product_loop_sale_flash - 10
             * @hooked woocommerce_template_loop_product_thumbnail - 10
             */
            do_action('woocommerce_before_shop_loop_item_title');

            ?>
        </div>
        <div class="products-content">
            <h3 class="product-title"><a href="<?php esc_url(the_permalink()); ?>"><?php esc_html(the_title()); ?></a>
            </h3>
            <div class="facade_type"> <?php $terms = wp_get_post_terms(
                    get_the_ID(),
                    'fasad_type',
                    array("fields" => "names")
                );
                foreach ($terms as $term) {
                    echo __('Тип фасаду: ', 'btsm');
                    echo $term;
                }
                ?> </div>
            <h4><?php $btsm_title = get_post_meta(get_the_ID(), 'btsm_title', true);
                echo esc_html($btsm_title); ?></h4>
            <div class="bmf_excerpt">
                <div itemprop="description">
                    <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?>
                </div>

                <a class="detailed" href="<?php echo get_permalink($product_id) ?>"><span
                            class="btn_text">Детальніше</span></a>
            </div>
            <!-- <div class="add-links-wrap">
				<div class='product-button'>
					<?php do_action('woocommerce_after_shop_loop_item'); ?>
				</div>
			</div> -->
        </div>
    </div>
</li>